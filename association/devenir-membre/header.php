<header><?php
    get_progressbar();
    get_menu();?>
    <div class="aside features container">
        <div class="row">
            <div class="col-12 offset-lg-4 col-lg-12 illustration aside">
                <picture>
                    <source srcset="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-light-1024w.webp 1024w, <?php get_website_url(); ?>img/bonbon-line-online-science-learning-light-768w.webp 768w" type="image/webp">
                    <source srcset="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-light-1024w.png 1024w, <?php get_website_url(); ?>img/bonbon-line-online-science-learning-light-768w.png 768w" type="image/png">
                    <img class="parallax light" style="min-height: 480px; height: 36vw; max-height: 680px" src="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-light.png" width="1024" height="768" alt="bonbon-line-online-science-learning présentant une fenêtre de programme informatique et des mains collaborant à la construction d'un univers composé de formes géométriques et de composants d'interface.">
                </picture>
                <picture>
                    <source srcset="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-dark-1024w.webp 1024w, <?php get_website_url(); ?>img/bonbon-line-online-science-learning-dark-768w.webp 768w" type="image/webp">
                    <source srcset="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-dark-1024w.png 1024w, <?php get_website_url(); ?>img/bonbon-line-online-science-learning-dark-768w.png 768w" type="image/png">
                    <img class="parallax dark" style="min-height: 480px; height: 36vw; max-height: 800px" src="<?php get_website_url(); ?>img/bonbon-line-online-science-learning-light.png" width="1024" height="768" alt="bonbon-line-online-science-learning présentant une fenêtre de programme informatique et des mains collaborant à la construction d'un univers composé de formes géométriques et de composants d'interface.">
                </picture>
            </div>
            <div class="col-12 col-lg-8 main-title">
                <h1>Devenir membre</h1>
                <p class="lead">Il est très simple de devenir membre de l’association, remplissez simplement le formulaire ci-dessous. La cotisation annuelle est de 12.- CHF pour un membre individuel et de 36.- CHF pour une personne morale ou si vous souhaitez soutenir l’association de façon plus significative, à envoyer par virement bancaire au compte ci-dessous. Si vous souhaitez payer par un autre moyen merci de contacter support@fairsocialnet.ch</p>
            </div>
        </div>
    </div>
</header>