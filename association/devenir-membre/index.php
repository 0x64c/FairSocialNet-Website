<?php
$websiteRootDir = '/fairsocialnet';
$websiteRootPath = $_SERVER["DOCUMENT_ROOT"].$websiteRootDir;
$currentPath = dirname(__FILE__);
$websiteLoadPath = '/php/load.php';
require_once($websiteRootPath.$websiteLoadPath);
get_head();?>
<body class="default"><?php
get_header($currentPath);?>
    <main>
        <section class="tiles module">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tile half-height">
                            <div class="content">
                                <div id="mastodon-vs-twitter" class="media">
                                    <picture>
                                        <source srcset="<?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-light-1024w.webp 1024w, <?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-light-768w.webp 768w" type="image/webp">
                                        <source srcset="<?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-light-1024w.png 1024w, <?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-light-768w.png 768w" type="image/png">
                                        <img class="light" src="<?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-light.png" width="1024" height="768" alt="Illustration présentant une fenêtre de programme informatique et des mains collaborant à la construction d'un univers composé de formes géométriques et de composants d'interface.">
                                    </picture>
                                    <picture>
                                        <source srcset="<?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-dark-1024w.webp 1024w, <?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-dark-768w.webp 768w" type="image/webp">
                                        <source srcset="<?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-dark-1024w.png 1024w, <?php get_website_url(); ?>img/bonbon-line-fast-email-sending-2-dark-768w.png 768w" type="image/png">
                                        <img class="dark" src="<?php get_website_url(); ?>img/illustration-light.png" width="1024" height="768" alt="Illustration présentant une fenêtre de programme informatique et des mains collaborant à la construction d'un univers composé de formes géométriques et de composants d'interface.">
                                    </picture>
                                </div>
                                <article class="text split figure">
                                    <main>
                                        <!-- <script type="text/javascript" src="https://webform.statslive.info/ow/eyJpdiI6IkdXd2J4ZkhsemxxdWY0MVZHalFHYzMyNFNlejltMXlxZEpZaUNNWm5NUGM9IiwidmFsdWUiOiJBc0J3S0pkMGplRHA2QjVDMVprcTR6RzFGZVY0RHFzMGsxMGFUMEFxbWc0PSIsIm1hYyI6Ijg0ODU5ZDFlOTBmMDFjOWIzY2QwNTVlOTMxYmYzNzIzNzU0ZmM5Njg1ZThkNjgyNzA2OTgxMDYxMjEyMzVkYzgifQ=="></script> -->
                                        <form method="post" action="//newsletter.infomaniak.com/external/submit" class="inf-form" target="_blank"><input type="email" name="email" style="display:none"><input type="hidden" name="key" value="eyJpdiI6ImJFWDhPNzBTbCtwNmVCaXJFdlE1UFpzcGVmZU5DUVwvb0Y0bkhTODVJZHg4PSIsInZhbHVlIjoidFBydEZcL081bllDaVVhSjRhdzVTMFIzalFBUFRBUFV0V0pzcWNFMEMzRFU9IiwibWFjIjoiYWVkZDlmNDU1NzE4MGFmZTczZWJkYTJmNmEwYmYyMWVjNTVkY2IyM2FjNmJkNDUwYWVkNjY2NDBmYjM3YTY0YyJ9"><input type="hidden" name="webform_id" value="4074">
                                        <style>
                                            .inf-main * {
                                                color: var(--primary-b-1-default)!important;                
                                            }                            .inf-main{ background-color:transparent; padding:25px 20px; margin:25px auto; } .inf-main .inf-content {} .inf-main h4, .inf-main span, .inf-main label, .inf-main input, .inf-main .inf-submit, .inf-main .inf-success p a { color:#555555; font-size:14px; } .inf-main h4{ font-size:18px; margin:0px 0px 13px 0px; } .inf-main h4, .inf-main label{ font-weight:bold; } .inf-main .inf-input { margin-bottom:7px; } .inf-main label { display:block;} .inf-main input{ height:35px; color:#999999; border: 1px solid #E9E9E9; border:none; padding-left:7px; } .inf-main .inf-input.inf-error label, .inf-main .inf-input.inf-error span.inf-message{ color: #cc0033; } .inf-main .inf-input.inf-error input{ border: 1px solid #cc0033; } .inf-main .inf-input input { border-radius: 4px; width:100%; background: var(--primary-b-1-default);} .inf-main .inf-input.inf-error span.inf-message { display: block; } .inf-main .inf-submit { text-align:right;} .inf-main .inf-submit input { background: var(--primary-b-1-default); color: var(--primary-a-1-default)!important; border-radius: 24px; border:none; font-weight: normal; height:auto; padding:7px; padding: 12px; font-weight: var(--font-weight-6); font-size: 1rem; letter-spacing: -0.02em;} .inf-main .inf-submit input.disabled{ opacity: 0.4; } .inf-btn { color: rgb(85, 85, 85); border: medium none; font-weight: normal; height: auto; padding: 7px; display: inline-block; background-color: white; box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.24); border-radius: 2px; line-height: 1em; } .inf-rgpd { margin:25px 0px 15px 0px; color:#555555; }
                                        </style>
                                            <div class="inf-main">
                                                <h4>Devenir membre</h4> <span>Pour vous inscrire comme membre actif de l'association</span>
                                                <div class="inf-success" style="display:none">
                                                    <h4>Votre inscription a été enregistrée avec succès !</h4>
                                                    <p> <a href="#" class="inf-btn">«</a> </p>
                                                </div>
                                                <div class="inf-content">
                                                    <div class="inf-input inf-input-text"> <input type="text" name="inf[1]" data-inf-meta="1" data-inf-error="Merci de renseigner une adresse email" required="required" placeholder="Email"> </div>
                                                    <div class="inf-input inf-input-text"> <input type="text" name="inf[3]" data-inf-meta="3" data-inf-error="Merci de renseigner une chaine de caractère" required="required" placeholder="Nom"> </div>
                                                    <div class="inf-input inf-input-text"> <input type="text" name="inf[2]" data-inf-meta="2" data-inf-error="Merci de renseigner une chaine de caractère" required="required" placeholder="Prénom"> </div>
                                                    <div class="inf-input inf-input-text"> <input type="text" name="inf[13572]" data-inf-meta="13572" data-inf-error="Ce champ est requis" placeholder="Adresse"> </div>
                                                    <div class="inf-rgpd">Votre adresse de messagerie est uniquement utilisée pour vous envoyer notre lettre d'information ainsi que des informations concernant nos activités. Vous pouvez à tout moment utiliser le lien de désabonnement intégré dans chacun de nos mails.</div>
                                                    <div class="inf-submit"> <input type="submit" name="" value="Devenir membre"> </div>
                                                </div>
                                            </div>
                                        </form>
                                    </main>
                                    <footer></footer>
                                </article>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="tile">
                            <div class="content">
                                <div class="media">
                                </div>
                                <article class="text">
                                    <header>
                                        <span>Liberapay</span>
                                    </header>
                                    <main>
                                        <p class="text-muted">Les personnes qui contribuent aux bien communs ont besoin de votre soutien. Construire des logiciels libres, diffuser les savoirs libres, ces choses prennent du temps et coûtent de l’argent, non seulement pour effectuer le travail initial, mais aussi pour le maintenir dans le temps.</p>
                                    </main>
                                </article>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="tile">
                            <div class="content">
                                <div class="media">
                                </div>
                                <article class="text">
                                    <header>
                                        <span>Liberapay</span>
                                    </header>
                                    <main>
                                        <img src="<?php get_website_url(); ?>img/TWINT_Montant-personalise_FR.png" class="w-100" alt="">
                                    </main>
                                </article>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="tile">
                            <div class="content">
                                <div class="media">
                                </div>
                                <article class="text">
                                    <header>
                                        <span>Liberapay</span>
                                    </header>
                                    <main>
                                        <p class="text-muted">Le système de dons récurrents de Liberapay est conçu pour fournir un revenu de base stable aux créateurs, leur permettant de continuer à travailler sur leurs projets qui sont bénéfiques pour tout le monde. Prêt à contribuer ? Alors allons-y :</p>
                                    </main>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="tile">
                            <div class="content">
                                <article class="text">
                                    <header>
                                        <span>PayPal</span>
                                    </header>
                                    <main>
                                        <p class="text-muted">PayPal est un service de paiement en ligne qui permet de recevoir des paiements. Pour donner cliquez sur le bouton ci-dessous.</p>
                                    </main>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="tile">
                            <div class="content">
                                <div class="media">
                                </div>
                                <article class="text">
                                    <header>
                                        <span>Virement banquaire</span>
                                    </header>
                                    <main>
                                        <p class="text-muted">Envoyez votre donation directement sur le compte en banque de l’association: IBAN: CH9800766000103126001
                                        Adresse :
                                        Association Fairsocialnet
                                        Rue du râteau 4a
                                        2000 Neuchâtel</p>
                                    </main>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><?php
    get_footer();?>
</body>
</html>